import { NgModule } from '@angular/core';
import { SelectComponent } from './select.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared/shared.module';
import { CommonModule } from '@angular/common';


@NgModule({
    declarations: [SelectComponent],
    imports: [
        NgSelectModule,
        FormsModule,
        SharedModule,
        CommonModule
        
    ],
    exports: [
        SelectComponent
    ]
})
export class SelectModule { }
