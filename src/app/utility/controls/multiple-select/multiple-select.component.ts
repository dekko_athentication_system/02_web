import { Component, OnInit, Input, Output, EventEmitter, forwardRef, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ControlItem, Value } from '../../utils/form';


@Component({
    selector: 'app-multiple-select',
    templateUrl: './multiple-select.component.html',
    styleUrls: ['./multiple-select.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => MultipleSelectComponent),
            multi: true
        }
    ]
})
export class MultipleSelectComponent implements OnInit, ControlValueAccessor {
    @Input() items: ControlItem[];
    @Input() placeholder: string;
    @Output() changed = new EventEmitter<Value[]>();

    isDisabled: boolean;
    value: Value[];
    currentLanguage:any;

    constructor(private translate: TranslateService) { }

    ngOnInit(): void {
        this.currentLanguage = this.translate.currentLang;

        this.translate.onLangChange.subscribe((s: { lang: string }) => {
            if (s.lang === 'en') {
              this.currentLanguage = 'en';
            } else {
              this.currentLanguage = 'bn';
            }
          });
    }

    private propagateChange: any = () => { };
    private propagateTouched: any = () => { };

    writeValue(value: Value[]): void {
        this.value = value;
    }

    registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.propagateTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }

    onChanged(event: any): void {
       this.value = event.map(
            (i: any)=> {  
              return i.value;
            
            });
        this.propagateChange(this.value);
        this.changed.emit(this.value);
    }

    onBlur(): void {
        this.propagateTouched();
    }

}
