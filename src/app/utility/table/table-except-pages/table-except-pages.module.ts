import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@app/shared/material.module';
import { ActionButtonsComponent } from '../action-buttons/action-buttons.component';
import { EllipsisButtonsComponent } from '../ellipsis-buttons/ellipsis-buttons.component';
import { SharedModule } from '@app/shared/shared.module';
import { TableExceptPagesComponent } from './table-except-pages.component';



@NgModule({
  declarations: [TableExceptPagesComponent],
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule
  ],
  exports: [TableExceptPagesComponent]
})
export class TableExceptPagesModule { }
