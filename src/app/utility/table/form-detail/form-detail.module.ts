import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormDetailComponent } from './form-detail.component';
import { SharedModule } from '@app/shared/shared.module';
import { ButtonModule } from '@app/utility/buttons/button/button.module';
import { MaterialModule } from '@app/shared/material.module';

@NgModule({
  declarations: [FormDetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    ButtonModule,
    MaterialModule
  ],
  exports: [FormDetailComponent]
})
export class FormDetailModule { }
