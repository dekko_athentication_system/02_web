import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule } from '@angular/forms';
import { LoadingImageModule } from '@app/core/modules/loading-image/loading-image.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    LoadingImageModule,
    TranslateModule,
    ReactiveFormsModule,
  ],
  exports: [
    ReactiveFormsModule,
    TranslateModule,
    LoadingImageModule
  ]
})
export class SharedModule { }
