import { Component, OnInit } from '@angular/core';
import { SwUpdate, VersionReadyEvent } from '@angular/service-worker';
import { TranslateService } from '@ngx-translate/core';
import { filter, map } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    
  constructor(translate: TranslateService, private swUpdate: SwUpdate) {
    translate.addLangs(['en', 'bn']);
    translate.setDefaultLang('en');
    translate.use('en');
  }

  ngOnInit(): void {
    const updatesAvailable = this.swUpdate.versionUpdates.pipe(
        filter((evt): evt is VersionReadyEvent => evt.type === 'VERSION_READY'),
        map(evt => ({
          type: 'UPDATE_AVAILABLE',
          current: evt.currentVersion,
          available: evt.latestVersion,
      })));
      updatesAvailable.subscribe((e) => {
        console.log(e)
          if(confirm("New version available. Load New Version?")) {
              window.location.reload();
          }
      })
      // if (this.swUpdate.isEnabled) {
      //     this.swUpdate.available.subscribe(() => {
      //         if(confirm("New version available. Load New Version?")) {
      //             window.location.reload();
      //         }
      //     });
      // }        
}


}
