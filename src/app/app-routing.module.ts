import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/auth/auth.guard';
import { Roles } from './core/auth/role';
import { RoleGuard } from './core/auth/role.guard';
import { CustomPreloadingStrategy } from './core/services/custom-preloading-strategy';
import { UserInfoResolver } from './core/services/user-info.resolver';
import { MainShellComponent } from './layout/main-shell/main-shell.component';

const routes: Routes = [
  {
    path: '',
    component: MainShellComponent,
    // resolve: { userInfo: UserInfoResolver },
    // canActivate: [AuthGuard],
    children: [
      {
        path: 'form',
        loadChildren: () => import("./features/example/form/form.module").then(m => m.ExampleModule),
        // canActivate: [RoleGuard],
        // data: { preload: true, delay: 3000 , roles: [Roles.CallCenterAgent, Roles.Bdex]},
      },
      {
        path: 'dashboard',
        loadChildren: () => import("./features/dashboard/dashboard.module").then(m => m.DashboardModule)
      },
      {
        path: 'datatable',
        loadChildren: () => import("./features/example/datatable/datatable.module").then(m => m.DatatableModule),
        // data: { preload: true, delay: 3000 },
      },
      {
        path: 'not-found',
        loadChildren: () => import("./core/modules/page-not-found/page-not-found.module").then(m => m.PageNotFoundModule),
      },
      {
        path: 'access-denied',
        loadChildren: () => import("./core/modules/access-denied/access-denied.module").then(m => m.AccessDeniedModule),
      },
      {
        path: '', redirectTo: '/dashboard', pathMatch: 'full'
      },
      {
        path: 'registration',
        loadChildren: () => import("./features/registration/registration.module").then(m => m.RegistrationModule),

      },
    ]
  },
  {
     path: 'login',
     loadChildren: () => import("./features/login/login.module").then(m => m.LoginModule),
    //  data: { preload: true },
  },
  {
    path: 'session-expired',
    loadChildren: () => import("./core/modules/session-expired/session-expired.module").then(m => m.SessionExpiredModule),
  },
  { 
     path: '**',
     redirectTo: 'not-found', pathMatch: 'full' 
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: CustomPreloadingStrategy})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
