import { Icon } from "@app/utility/utils/form";

export interface Item {
    id: string;
    name: string;
    icon?: Icon;
}