import { Injectable } from '@angular/core';
import {
    CanActivate, CanActivateChild, CanLoad, Route, UrlSegment, ActivatedRouteSnapshot,
    RouterStateSnapshot, UrlTree, Router
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { SharedService } from '../services/shared.service';

@Injectable({
    providedIn: 'root'
})
export class RoleGuard implements CanActivate, CanActivateChild, CanLoad {

    constructor(private router: Router, private sharedService: SharedService) {
    }

    private check(allowedRoles: string[]): Observable<boolean> {
        console.log("Rule GUARD");
        let currentRole = this.sharedService.getCurrentUserRole();
        if(allowedRoles.includes(currentRole)) {
            return of(true);
        }
        this.router.navigate(['/access-denied']);
        return of(false);
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return this.check(next.data.roles);
    }
    canActivateChild(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return this.check(next.data.roles);
    }
    canLoad(
        route: Route,
        segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
        return this.check(route.data?.roles);
    }
}
