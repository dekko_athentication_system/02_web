export interface Role {
    name: string;
}

export enum Roles {
    Admin = 'ADMIN',
    Agent = 'AGENT',
    SuperAdmin = 'SUPER ADMIN',
    CallCenterAgent = 'CALL CENTER AGENT',
    Bdex = 'BDEX'
};