export interface AuthModel {
    readonly user_name: string;
    readonly user_id: string;
    readonly role_name: string;
    readonly web_json: string;
    readonly reset_required: string;
}