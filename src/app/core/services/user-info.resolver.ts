import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { MainShellComponent } from '@app/layout/main-shell/main-shell.component';
import { catchError, forkJoin, Observable, of } from 'rxjs';
import { UserService } from '../../features/services/user/user.service';
import { SessionService } from './session.service';

@Injectable({
  providedIn: 'root'
})
export class UserInfoResolver implements Resolve<MainShellComponent> {

  constructor(private userService: UserService, private sessionService: SessionService) {
    
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    return this.userService.getUserProfileInfo('test').pipe(
        catchError((error) => {
          this.sessionService.logout();
          return of(null);
        })
    );
  }
}
