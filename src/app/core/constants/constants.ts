import { HttpHeaders } from '@angular/common/http';
import { ChangeDetectorRef, inject, ViewRef } from '@angular/core';
import { TableColumn } from '@app/utility/utils';
import { Observable, ReplaySubject, takeUntil, UnaryFunction } from 'rxjs';

export const CONTENT_TYPE = 'application/json';
export const REQUEST_TIMEOUT_IN_SECONDS = '30';
export const nidNoRegEx =  '^([0-9]{10}|[0-9]{13}|[0-9]{17})$';
export const passportNoRegEx =  '^([0-9]{7})$';
export const birthDrivingNoRegEx =  '^([0-9]{8,35})$';
export const mobileNoRegEx = '^(0|[0+]880)1[3-9][0-9]{8}$';
export const AUTH_STORE_KEY = 'accesstoken.ims.cookie.store.key';
export const AUTH_MENU_JSON_KEY = 'accesstoken.dekko.menu.json.key';
export const AUTH_USER_ID_KEY = 'accesstoken.dekko.user.id.key';
export const AUTH_PASSWORD_KEY = 'accesstoken.dekko.password.key';
export const AUTH_ROLE_ID_KEY = 'accesstoken.dekko.roleId.key';
export const AUTH_IS_LOGGED_IN_KEY = 'accesstoken.dekko.isLoggedIn.key';

export const REGISTRATION_REF_ID = 'registrationRefId';
export const OTP_EXPIRES_AT = 'otpExpiresAt';
export const USER_ID = 'userId';
export const PASSWORD = 'password';
export const SAVE_ID = 'saveId';
export const REF_ID = 'refId';
export const OTP_KEY = 'otpKey';
export const REGEX_EMAIL = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z0-9.-]{1,}$/;
export const CUSTOMER_ID_REGEX = /[0-9]{8}/;
export const ACCOUNT_NO_REGEX = /^([0-9\-]{14}|[0-9\-]{15})$/;
export const MOBILE_NO_REGEX = /^((01[3-9]{1})[0-9]{8})$/;
export const MOBILE_NO_REGEX_V2 = /^((\+88)|(\+8801[3-9]{1})[0-9]{8})$/;
export const NID_REGEX = /^(\d{10}|\d{13}|\d{17})$/;
export const PASS_REGEX =/^([\w\W]{8})/ ;
export const EMAIL_REGEX = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z0-9.-]{1,}$/;
export const DEFAULT_OFFSET = 0;
export const DEFAULT_LIMIT = 10;


export function getHttpHeaders(): HttpHeaders {
    return new HttpHeaders()
        .set('content-type', CONTENT_TYPE)
        .set('accept', CONTENT_TYPE);
}


export function getColumns(keys: any): TableColumn[] {
    let columns: TableColumn[] = [];
    keys.forEach((k: any, i: number) => {
        let col = new TableColumn();
        if(k=== 'oid') return;
        col.columnDef = k;
        col.header =  k;
        columns.push(col);
    });
    return columns;
}


export function takeUntilDestroy$<T>(): UnaryFunction<Observable<T>, Observable<T>> {
    const viewRef = inject(ChangeDetectorRef) as ViewRef;
    const destroyer$ = new ReplaySubject<void>(1);

    viewRef.onDestroy(() => destroyer$.next())
    
    return (observable: Observable<T>) => observable.pipe(takeUntil(destroyer$));
  }
  