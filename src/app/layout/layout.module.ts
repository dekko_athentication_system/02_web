import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import { MainShellComponent } from './main-shell/main-shell.component';
import { MaterialModule } from '@app/shared/material.module';
import { SideNavbarComponent } from './side-navbar/side-navbar.component';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  declarations: [SidebarComponent, SideNavbarComponent, FooterComponent,MainShellComponent],
  exports: [SidebarComponent, SideNavbarComponent, FooterComponent],
  imports: [
    CommonModule, MaterialModule, TranslateModule, RouterModule, HttpClientModule, HttpClientJsonpModule
  ]
})
export class LayoutModule { }
