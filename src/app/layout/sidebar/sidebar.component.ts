 
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core'
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AUTH_IS_LOGGED_IN_KEY, AUTH_ROLE_ID_KEY, AUTH_USER_ID_KEY } from '@app/core/constants/constants';
import { SessionService } from '@app/core/services/session.service';
import { SharedService } from '@app/core/services/shared.service';
import { UserService } from '@app/features/services/user/user.service';
import { WebsocketService } from '@app/features/websocket/services/websocket.service';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
declare var $: any;
 
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
})
export class SidebarComponent implements OnInit {
  mobileQuery: MediaQueryList
  
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  menuList: any[] = [];
  userName: string;  role: string;
  language: String; userNameBn: string;
  socketService: WebsocketService;
  roleId: any;

  public userId: any = {};
  public isLoggedIn: any = {};


  constructor(private breakpointObserver: BreakpointObserver,
              public sessionService: SessionService,
              private sharedService: SharedService,
              private route: ActivatedRoute,
              private translate: TranslateService,
              private userService: UserService,) {
                this.language = translate.currentLang;
              }

  
  
  ngOnInit() {
    this.roleId = localStorage.getItem(AUTH_ROLE_ID_KEY)
    this.roleId = this.roleId.replaceAll('"','');

    this.userId['userId'] = localStorage.getItem(AUTH_USER_ID_KEY)
    this.userId['userId'] = this.userId['userId'].replaceAll('"','');
    console.log(this.userId)

    this.userService.getUserProfileInfo(this.userId)
        .subscribe(data => {
          let { user_name, userId, roleNameEn, menuJson, user_name_bn, oid, extension_number} = data.body.data;
                this.userName = user_name;
                this.userNameBn = user_name_bn;
                // this.userId = userId;
                this.role = this.roleId;
                this.menuList = JSON.parse(data.body.data.menuJson);
                this.sharedService.setCurrentUserRole(roleNameEn);
                this.sharedService.setcurrentLoggedInUserID(userId);
                this.sharedService.setUserLoginOid(oid);
                this.sharedService.setUserExtension(extension_number);
                if(roleNameEn === 'ADMIN'){
                  this.socketService = new WebsocketService(this.sharedService);
                  this.socketService.listen('getUserInfo').subscribe(result=> {
                    // this.getIssuerListByMobile(result.mobileNumber);
                  });
                }
        });
   }

   logout(): void {
    console.log(this.userId)
    this.userService.logout(this.userId)
        .subscribe(data => {
          console.log(data)
        });

     this.sessionService.logout();

    //  if(this.role === 'ADMIN'){
    //   this.socketService.disconnect();
    // }
   }

   
}
