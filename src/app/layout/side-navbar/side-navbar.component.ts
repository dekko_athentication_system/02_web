import { Component, Input, OnInit } from '@angular/core';
import { AUTH_USER_ID_KEY } from '@app/core/constants/constants';
import { SessionService } from '@app/core/services/session.service';
import { UserService } from '@app/features/services/user/user.service';
import { catchError, forkJoin, Observable, of } from 'rxjs';
// import { SessionService } from './session.service';
 
@Component({
  selector: 'app-side-navbar',
  templateUrl: './side-navbar.component.html',
  styleUrls: ['./side-navbar.component.css']
})
export class SideNavbarComponent implements OnInit {

  @Input() menuList: any[] = [];
  
  constructor(
    private userService: UserService,
    private sessionService: SessionService
  ) { }

  ngOnInit() {
    this.getInfo()
  }

  public userId : any;

  getInfo(){
    this.userId = localStorage.getItem(AUTH_USER_ID_KEY)
    return this.userService.getUserProfileInfo(this.userId).pipe(
      catchError((error) => {
        this.sessionService.logout();
        return of(null);
      })
  );
  }
}
