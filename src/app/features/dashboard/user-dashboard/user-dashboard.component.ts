import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService } from '@app/core/services/notification.service';
import { SharedService } from '@app/core/services/shared.service';
import { UtilService } from '@app/features/services/utils/util.service';
import { TranslateService } from '@ngx-translate/core';
import { ArcElement, BarController, BarElement, CategoryScale, Chart, LinearScale, LineController, LineElement, PieController, PointElement, Title } from 'chart.js';
import * as moment from 'moment';
import { ReplaySubject } from 'rxjs';
import { DashboardService } from '../service/dashboard.service';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css']
})
export class UserDashboardComponent implements OnInit {

  isLoading: boolean = false;
  date_times: any;
  public currentLanguage: string;
  private currentYear: any;

  submitted_data : any; 
  resolved_data : any; 
  ready_to_assign_data : any;
  current_year_resolved_data: any;
  current_year_submitted_data: any;
  pending_data: any;

  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);

  constructor(
    private dashboardService: DashboardService,
    private router: Router,
    private notificationService: NotificationService,
    private translate: TranslateService,
    public utilService: UtilService,
    private sharedService: SharedService
  ) {
    Chart.register(LineController, BarController, PieController, BarElement, LineElement, ArcElement, PointElement, LinearScale, Title, CategoryScale);
    setInterval(() => {
      this.date_times = moment().format('YYYY-MM-DD HH:mm:ss');
    }, 1000);
  }

  ngOnInit(): void {
    // this.currentYear = moment(new Date()).format('YYYY');
    // this.getCallCenterAgentContent();

    // this.currentLanguage = this.translate.currentLang;

    // this.translate.onLangChange.subscribe((s: { lang: string }) => {
    //   if (s.lang === 'en') {
    //     this.currentLanguage = 'en';
    //   } else {
    //     this.currentLanguage = 'bn';
    //   }
    //   this.barChart();
    //   this.barChartTwo();
    // });
  }

  getCreatedIssueList(){
    this.router.navigate(['/issue', {createdBy: this.sharedService.getUserLoginOid()}]);
  }

  getResolvedIssueList(){
    this.router.navigate(['/issue', {status: 'Resolved', resolvedBy: this.sharedService.getUserLoginOid()}]);
  }

  getPendingIssueList(){
    this.router.navigate(['/issue', {status: 'Submitted'}]);
  }

  getList(){
    this.router.navigate(['/issue']);
  }

  getCallCenterAgentContent() {
    this.dashboardService.getCallCenterAgentContent().subscribe({
        next: (res: any) => {          
          this.submitted_data = res.submitted_data;
          this.resolved_data = res.resolved_data;
          this.ready_to_assign_data = res.ready_to_assign_data;
          this.current_year_resolved_data = res.current_year_resolved_data;
          this.current_year_submitted_data = res.current_year_submitted_data;
          this.pending_data = res.pending_data;

          this.barChart();
          this.barChartTwo();
          this.isLoading = false;
        },
        error: (err) => {
          this.notificationService.errorSnackBar(err);
        },
        complete: () => { }
      });
  }

  private labelEn = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  private labelBn = ['জানুয়ারি', 'ফেব্রুয়ারি', 'মার্চ', 'এপ্রিল', 'মে', 'জুন', 'জুলাই', 'আগস্ট', 'সেপ্টেম্বর', 'অক্টোবর', 'নভেম্বর', 'ডিসেম্বর'];

  public barChartOne: any;
  barChart() {

    if(this.barChartOne){
      this.barChartOne.destroy();
   }

    const ctx = document.getElementById('barChart') as HTMLCanvasElement;
    this.barChartOne = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: this.currentLanguage == 'en'? this.labelEn: this.labelBn,
        datasets: [{
          label: 'Year',
          data: this.current_year_resolved_data.map((i: any)=> {  
            return i.total;
          }),
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            'rgba(255, 205, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(201, 203, 207, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            'rgba(255, 205, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(153, 102, 255, 0.2)',
          ],
          borderColor: [
            'rgb(255, 99, 132)',
            'rgb(255, 159, 64)',
            'rgb(255, 205, 86)',
            'rgb(75, 192, 192)',
            'rgb(54, 162, 235)',
            'rgb(153, 102, 255)',
            'rgb(201, 203, 207)',
            'rgb(255, 99, 132)',
            'rgb(255, 159, 64)',
            'rgb(255, 205, 86)',
            'rgb(75, 192, 192)',
            'rgb(54, 162, 235)',
            'rgb(153, 102, 255)',
          ],
          borderWidth: 2
        }]
      },
      options: {
        indexAxis: 'y',
        elements: {
          bar: {
            borderWidth: 3,
          }
        },
        responsive: true,
        plugins: {
          legend: {
            position: 'right',
          },
          title: {
            display: true,
            text: this.currentLanguage == 'en'? 'Resolved Issues\' summary ('+this.currentYear+')'
            :'সমাধান করা ইস্যুর সারসংক্ষেপ ('+this.utilService.translateDate(this.currentYear)+')'
          }
        }
      },
    });
  }

  public myBarChart: any;

  barChartTwo() {
   
// Destroys a specific chart instance
if(this.myBarChart){
   this.myBarChart.destroy();
}
    const ctx = document.getElementById('barChartTwo') as HTMLCanvasElement;
     this.myBarChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: this.currentLanguage == 'en'? this.labelEn: this.labelBn,
        datasets: [{
          label: 'Year',
          data: this.current_year_submitted_data.map((i: any)=> {  
            return i.total;
          }),
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            'rgba(255, 205, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(201, 203, 207, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            'rgba(255, 205, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(153, 102, 255, 0.2)',
          ],
          borderColor: [
            'rgb(255, 99, 132)',
            'rgb(255, 159, 64)',
            'rgb(255, 205, 86)',
            'rgb(75, 192, 192)',
            'rgb(54, 162, 235)',
            'rgb(153, 102, 255)',
            'rgb(201, 203, 207)',
            'rgb(255, 99, 132)',
            'rgb(255, 159, 64)',
            'rgb(255, 205, 86)',
            'rgb(75, 192, 192)',
            'rgb(54, 162, 235)',
            'rgb(153, 102, 255)',
          ],
          borderWidth: 2
        }]
      },
      options: {
        indexAxis: 'x',
        elements: {
          bar: {
            borderWidth: 3,
          }
        },
        responsive: true,
        plugins: {
          legend: {
            position: 'right',
          },
          title: {
            display: true,
            text: this.currentLanguage == 'en'? 'Submitted Issues\' summary ('+this.currentYear+')'
            :'জমা দেওয়া ইস্যুর সারসংক্ষেপ ('+this.utilService.translateDate(this.currentYear)+')'
          }
        }
      },
    });
  }

  
  ngOnDestroy(): void {
      this.destroyed$.next(true);
      this.destroyed$.complete();
  }

}
