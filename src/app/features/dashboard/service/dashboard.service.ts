import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ResourceService } from '@app/core/services/resouce.service';
import { QueryParams } from '@app/models';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService  extends ResourceService<any>{

  constructor(private http: HttpClient) {
    super(http);
 }

  getTeamLeadDataContent(): Observable<any> {
    return this.getList('v1/team-lead/data/content');
  }

  getTeamMemberDataContent(): Observable<any> {
    return this.getList('v1/team-member/data/content');
  }

  getCallCenterExecutiveDataContent(): Observable<any> {
    return this.getList('v1/team-lead/data/content');
  }

  getAdminDataContent(): Observable<any> {
    return this.getList('v1/admin/data/content');
  }

  getIssueCategoryList(params: QueryParams): Observable<any> {    
    return this.getListWithPage(params, 'v1/issue-list-dashboard');
  }

  getCallCenterAgentContent(): Observable<any> {
    return this.getList('v1/call-center-executive/data/content');
  }

}
