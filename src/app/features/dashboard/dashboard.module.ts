import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { SharedModule } from '@app/shared/shared.module';
import { DashboardComponent } from './dashboard.component';
import { UtilityModule } from '@app/utility/utility.module';
import { MaterialModule } from '@app/shared/material.module';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';


@NgModule({
  declarations: [
    AdminDashboardComponent,
    DashboardComponent,
    UserDashboardComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    DashboardRoutingModule,
    CommonModule,
    UtilityModule,
    MaterialModule,
    ]
})
export class DashboardModule { }
