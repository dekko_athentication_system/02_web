import { Component, OnInit } from '@angular/core';
import { AUTH_ROLE_ID_KEY } from '@app/core/constants/constants';
import { SharedService } from '@app/core/services/shared.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public userRole: any;

  constructor(private sharedService: SharedService) { }

  ngOnInit(): void {
    this.userRole = localStorage.getItem(AUTH_ROLE_ID_KEY)
    this.userRole = this.userRole.replaceAll('"','');
    console.log(this.userRole)
    // this.userRole = this.sharedService.getCurrentUserRole();
  }

}
