import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from '@app/core/services/notification.service';
import { SharedService } from '@app/core/services/shared.service';
import { DashboardService } from '../service/dashboard.service';
import * as moment from 'moment';
import { Chart, LineController, LineElement, PointElement, LinearScale, Title, CategoryScale, BarController, BarElement, PieController, ArcElement } from 'chart.js';
import { TranslateService } from '@ngx-translate/core';
import { UtilService } from '@app/features/services/utils/util.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {

  isLoading: boolean = false;
  date_times: any;

  private currentYear: any;
  public currentLanguage: string;


  submitted_data: any;
  resolved_data: any;
  total_issue_data: any;
  team_data: any;
  current_year_resolved_issue: any;
  current_year_Submitted_issue: any;

  constructor(
    private dashboardService: DashboardService,
    private router: Router,
    private notificationService: NotificationService,
    private activatedRoute: ActivatedRoute,
    private sharedService: SharedService,
    private translate: TranslateService,
    public utilService: UtilService
  ) {
    Chart.register(LineController, BarController, PieController, BarElement, LineElement, ArcElement, PointElement, LinearScale, Title, CategoryScale);
    setInterval(() => {
      this.date_times = moment().format('YYYY-MM-DD HH:mm:ss');
    }, 1000);
  }

  private labelEn = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  private labelBn = ['জানুয়ারি', 'ফেব্রুয়ারি', 'মার্চ', 'এপ্রিল', 'মে', 'জুন', 'জুলাই', 'আগস্ট', 'সেপ্টেম্বর', 'অক্টোবর', 'নভেম্বর', 'ডিসেম্বর'];


  ngOnInit(): void {
    // this.currentYear = moment(new Date()).format('YYYY');
    // this.getAdminData();

    // this.currentLanguage = this.translate.currentLang;

    // this.translate.onLangChange.subscribe((s: { lang: string }) => {
    //   if (s.lang === 'en') {
    //     this.currentLanguage = 'en';
    //   } else {
    //     this.currentLanguage = 'bn';
    //   }
    //   this.barChart();
    //   this.barChartTwo();
    // });
  }

  getAdminData() {
    this.isLoading = true;
    this.dashboardService.getAdminDataContent().subscribe({
      next: (res: any) => {
        this.submitted_data = res.submitted_data;
        this.resolved_data = res.resolved_data;
        this.total_issue_data = res.total_issue_data;
        this.team_data = res.team_data;

        this.current_year_Submitted_issue = res.current_year_Submitted_issue;
        this.current_year_resolved_issue = res.current_year_resolved_issue;

        this.barChart();
        this.barChartTwo();
        this.isLoading = false;
      },
      error: (err) => {
        this.notificationService.errorSnackBar(err);
      },
      complete: () => { }
    })
  }

  public barChartElement: any;

  barChart() {

    if (this.barChartElement) {
      this.barChartElement.destroy();
    }

    const ctx = document.getElementById('barChart') as HTMLCanvasElement;
    this.barChartElement = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: this.currentLanguage == 'en' ? this.labelEn : this.labelBn,
        datasets: [{
          label: 'Year',
          data: this.current_year_resolved_issue.map((i: any) => {
            return i.total;
          }),
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            'rgba(255, 205, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(201, 203, 207, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            'rgba(255, 205, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(153, 102, 255, 0.2)',
          ],
          borderColor: [
            'rgb(255, 99, 132)',
            'rgb(255, 159, 64)',
            'rgb(255, 205, 86)',
            'rgb(75, 192, 192)',
            'rgb(54, 162, 235)',
            'rgb(153, 102, 255)',
            'rgb(201, 203, 207)',
            'rgb(255, 99, 132)',
            'rgb(255, 159, 64)',
            'rgb(255, 205, 86)',
            'rgb(75, 192, 192)',
            'rgb(54, 162, 235)',
            'rgb(153, 102, 255)',
          ],
          borderWidth: 2
        }]
      },
      options: {
        indexAxis: 'y',
        elements: {
          bar: {
            borderWidth: 3,
          }
        },
        responsive: true,
        plugins: {
          legend: {
            position: 'right',
          },
          title: {
            display: true,
            text: this.currentLanguage == 'en'? 'Resolved Issues\' summary ('+this.currentYear+')'
            :'সমাধান করা ইস্যুর সারসংক্ষেপ ('+this.utilService.translateDate(this.currentYear)+')'
          }
        }
      },
    });
  }

  public barChartTwoElement: any
  barChartTwo() {
    if (this.barChartTwoElement) {
      this.barChartTwoElement.destroy();
    }
    const ctx = document.getElementById('barChartTwo') as HTMLCanvasElement;
    this.barChartTwoElement = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: this.currentLanguage == 'en' ? this.labelEn : this.labelBn,
        datasets: [{
          label: 'Year',
          data: this.current_year_Submitted_issue.map((i: any) => {
            return i.total;
          }),
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            'rgba(255, 205, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(201, 203, 207, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            'rgba(255, 205, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(153, 102, 255, 0.2)',
          ],
          borderColor: [
            'rgb(255, 99, 132)',
            'rgb(255, 159, 64)',
            'rgb(255, 205, 86)',
            'rgb(75, 192, 192)',
            'rgb(54, 162, 235)',
            'rgb(153, 102, 255)',
            'rgb(201, 203, 207)',
            'rgb(255, 99, 132)',
            'rgb(255, 159, 64)',
            'rgb(255, 205, 86)',
            'rgb(75, 192, 192)',
            'rgb(54, 162, 235)',
            'rgb(153, 102, 255)',
          ],
          borderWidth: 2
        }]
      },
      options: {
        indexAxis: 'x',
        elements: {
          bar: {
            borderWidth: 3,
          }
        },
        responsive: true,
        plugins: {
          legend: {
            position: 'right',
          },
          title: {
            display: true,
            text: this.currentLanguage == 'en'? 'Submitted Issues\' summary ('+this.currentYear+')'
            :'জমা দেওয়া ইস্যুর সারসংক্ষেপ ('+this.utilService.translateDate(this.currentYear)+')'
          }
        }
      },
    });
  }

  goList(){
    this.router.navigate(['/issue/admin-issue-list']);
  }

  getCreatedIssueList(){
    this.router.navigate(['/issue/admin-issue-list', {createdBy: this.sharedService.getUserLoginOid()}]);
  }

  getResolvedIssueList(){
    this.router.navigate(['/issue/admin-issue-list', {status: 'Resolved'}]);
  }

  getPendingIssueList(){
    this.router.navigate(['/issue/admin-issue-list', {status: 'Submitted'}]);
  }

}
