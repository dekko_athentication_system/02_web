import { TranslateService } from '@ngx-translate/core';
import { takeUntil, finalize } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ReplaySubject } from 'rxjs';
import { AUTH_IS_LOGGED_IN_KEY, AUTH_MENU_JSON_KEY, AUTH_PASSWORD_KEY, AUTH_ROLE_ID_KEY, AUTH_USER_ID_KEY } from '@app/core/constants/constants';
import { UserService } from '../services/user/user.service';

@Component({
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

    public isLoading: boolean = false;
    public loginForm: FormGroup;
    public showError: boolean = false; isAuthLoading: boolean = false;
    private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);

    constructor(private router: Router, private fb: FormBuilder, private userService: UserService,
        private translateService: TranslateService) {
    }

    onSubmit(): void {
        this.isAuthLoading = true;
        console.log(this.loginForm.value)
        // this.loginForm.setValue([isLoggedIn]) = 'Yes'
        this.loginForm.value.isLoggedIn = 'No'
        this.userService.login(this.loginForm.value)
        .pipe(takeUntil(this.destroyed$),
         finalize(() => { this.isAuthLoading = false;}))
         .subscribe((res: any) => {
            console.log(res)
            if(res.body.code === 200) {
                this.showError = false;
                localStorage.setItem(AUTH_MENU_JSON_KEY, JSON.stringify(res.body.data.menuJson));
                localStorage.setItem(AUTH_USER_ID_KEY, JSON.stringify(res.body.data.userId));
                localStorage.setItem(AUTH_PASSWORD_KEY, JSON.stringify(res.body.data.password));
                localStorage.setItem(AUTH_ROLE_ID_KEY, JSON.stringify(res.body.data.roleId));
                localStorage.setItem(AUTH_IS_LOGGED_IN_KEY, JSON.stringify(res.body.data.isLoggedIn));
                this.router.navigate(['dashboard']);
                console.log("hello")
            } else {
                this.showError = true;
            }
        });
    }

    closeError() {
        this.showError = false;
    }

    forgotPass() {
        this.router.navigate(['/forgot-userid-password']);
    }

    changeLang(lang: string) {
        this.translateService.use(lang);
        localStorage.setItem('currentLang', lang);
    }

    // speak(msg: string) {
    //     console.log('speak')
    //     const sp = new SpeechSynthesisUtterance(msg);
    //     [sp.voice] = speechSynthesis.getVoices();
    //     speechSynthesis.speak(sp);
    // }


    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

    ngOnInit(): void {
        this.loginForm = this.fb.group({
            userId: ['', Validators.required],
            password: ['', Validators.required]
        })
    }
}
