import { HttpBackend, HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ResourceService } from '@app/core/services/resouce.service';
import { QueryParams } from '@app/models';
import { Member } from '@app/models/member';
import { catchError, map, Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DatatableService extends ResourceService<any> {

  constructor(private http: HttpClient) { 
    super(http);
  }
}
