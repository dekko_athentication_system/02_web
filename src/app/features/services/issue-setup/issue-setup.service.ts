import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend } from '@angular/common/http';
import { Observable } from 'rxjs';

import { getHttpHeaders } from '../../../core/constants/constants';
import { environment } from '@env';

@Injectable({
    providedIn: 'root'
})
export class IssueSetupService {

    constructor(private http: HttpClient, private backend: HttpBackend) {
     }

    getIssuecategoryList(search_params? : any): Observable<any> {
        return this.http.post(`${environment.BASE_URL}/v1/issue-categories`, search_params, {
            headers: getHttpHeaders(),
            observe: 'response'
        });
    }
}
