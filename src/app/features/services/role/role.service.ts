import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpBackend } from '@angular/common/http';
import { Observable } from 'rxjs';

import { getHttpHeaders } from '../../../core/constants/constants';
import { ResourceService } from '../../../core/services/resouce.service';
import { AuthModel } from '../../../core/auth/auth.model';
import { environment } from '@env';

@Injectable({
    providedIn: 'root'
})
export class RoleService extends ResourceService<any>{

    constructor(private http: HttpClient) {
        super(http);
     }

     getRoleList(): Observable<any> {
        return this.getList('v1/role-list');
      }

}
