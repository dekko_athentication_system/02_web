import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpBackend } from '@angular/common/http';
import { Observable } from 'rxjs';

import { getHttpHeaders } from '../../../core/constants/constants';
import { ResourceService } from '../../../core/services/resouce.service';
import { AuthModel } from '../../../core/auth/auth.model';
import { environment } from '@env';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(private http: HttpClient, private backend: HttpBackend) {
     }

    //  toServerModel(entity: AuthModel): any {
    //     return {
    //       ...entity,
    //     //   createDate: entity.createDate.getTime()
    //     };
    //   }
    
    //   fromServerModel(json: any): AuthModel{
    //     return {
    //       ...json,
    //       createDate: new Date(json.createDate)
    //     };
    //   }

    getUserProfileInfo(userId: any): Observable<any> {
        let http = new HttpClient(this.backend);
        return this.http.post(`${environment.BASE_URL}/v1/login/user-info`, userId, {
            // headers: getHttpHeaders(),
            observe: 'response'
        });
    }

    login(credentials: any): Observable<any> {
        let http = new HttpClient(this.backend);
        return http.post(`${environment.BASE_URL}/v1/login/login`, credentials, {
            // headers: getHttpHeaders(),
            observe: 'response'
        });
    }

    logout(userId: any): Observable<any> {
        let http = new HttpClient(this.backend);
        return this.http.post(`${environment.BASE_URL}/v1/login/logout`, userId, {
            // headers: getHttpHeaders(),
            observe: 'response'
        });
    }

}
