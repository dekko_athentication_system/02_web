import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DatatableRoutingModule } from './datatable-routing.module';
import { UtilityModule } from '@app/utility/utility.module';
import { ReactiveFormsModule } from '@angular/forms';
import { DatatableComponent } from './datatable.component';
import { SharedModule } from '@app/shared/shared.module';


@NgModule({
  declarations: [
    DatatableComponent
  ],
  imports: [
    CommonModule,
    UtilityModule,
    DatatableRoutingModule
  ]
})
export class DatatableModule { }
