import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Sort } from '@angular/material/sort';
import { DEFAULT_OFFSET, DEFAULT_LIMIT, getColumns } from '@app/core/constants/constants';
import { NotificationService } from '@app/core/services/notification.service';
import { QueryParams } from '@app/models';
import { Member } from '@app/models/member';
import { Pageable, TableColumn } from '@app/utility/utils';
import { Observable } from 'rxjs';
import { DatatableService } from '../../services/datatable/datatable.service';

@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatatableComponent implements OnInit {

  columns: TableColumn[] = []; 
  data: any[] = [];
  totalCount = 0;
  isLoading: boolean = false;
  isPageIndexZero: boolean = false;
  constructor(private datatableService: DatatableService, private notificationService: NotificationService,
              private http: HttpClient, private ref: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.getList();
  }

  getList(offset = DEFAULT_OFFSET, limit = DEFAULT_LIMIT, searchText: string = '', sortOrderId: string= 'memberid', sortOrder: string = 'asc'): void {
      // let params = new QueryParams(offset, limit, '', searchText, sortOrder, sortOrderId);
      // this.datatableService.getList('v1/issue-categories')
      // .subscribe({
      //   next: (res: any) => {
      //     console.log(res)
      //     this.data = res.data;
      //     this.totalCount = res.count;
      //   },
      //   error: (err) => {
      //     this.notificationService.errorSnackBar(err);
      //   },
      //   complete: () => {} 
      // });
      this.getJSON().subscribe(res => {
        this.data = res.data;
        this.totalCount = res.totalCount;
        if(this.data.length > 0) {
          this.columns = [];
          this.columns = getColumns(Object.keys(this.data[0]))
        }
        this.ref.detectChanges();
      });
}

  public getJSON(): Observable<any> {
    return this.http.get("./assets/list.json");
  }

  pageChangeEvent(event: Pageable): void  {
    this.getList(event.offset, event.limit);
  }

  pageSortEvent(event: Sort): void  {
    this.getList(DEFAULT_OFFSET, DEFAULT_LIMIT, '', event.active, event.direction);
  }

  onTableAction(event: any): void {
    console.log('event', event);
  }

  onSearch(searchText: string) {
    console.log("search click");
    this.isPageIndexZero = true;
    this.getList(DEFAULT_OFFSET, DEFAULT_LIMIT, searchText);
  }

}
