import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NotificationService } from '@app/core/services/notification.service';
import { ControlItem } from '@app/models';
import { markFormGroupTouched } from '@app/utility/utils/form';
import { regexErrors, regex } from '@app/utility/utils/regex';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  form: FormGroup;
  regexErrors = regexErrors;
  items: ControlItem[];
  isInline: boolean;
  constructor(private notificationService: NotificationService, private fb: FormBuilder,) {
    this.isInline = false;
    this.items = [
      { label: 'First', labelBn: 'First', value: 1 },
      { label: 'Second', labelBn: 'Second', value: 2 },
      { label: 'Third', labelBn: 'Third', value: 3 },
      { label: 'Fourth', labelBn: 'Fourth', value: 4 },
      { label: 'Fifth', labelBn: 'Fifth', value: 5 }
    ];
   }

  ngOnInit(): void {
    this.form = this.fb.group({
      email: [null, {
          updateOn: 'blur', validators: [
              Validators.required,
              Validators.maxLength(128),
              Validators.pattern(regex.email)
          ]
      }],
      password: [null, {
          updateOn: 'change', validators: [
              Validators.required
          ]
      }],
      date: [null, {
        updateOn: 'change', validators: [
            Validators.required
        ]
      }],
      select: [null, {
        updateOn: 'change', validators: [
            Validators.required
        ]
      }],
      checkboxes: [null, {
        updateOn: 'change', validators: [
            Validators.required
          ]
      }],
      radios: [null, {
          updateOn: 'change', validators: [
              Validators.required
          ]
      }],
      textarea: [null, {
        updateOn: 'change', validators: [
            Validators.required
        ]
      }],
    });
  }

  public onSubmit(): void {
    console.log(this.form.value);
    if (this.form.valid) {
        const value = this.form.value;
       
        // const credentials: fromUser.EmailPasswordCredentials = {
        //     email: value.email,
        //     password: value.password
        // };
        // this.store.dispatch(new fromUser.SignInEmail(credentials));

    } else {
        markFormGroupTouched(this.form);
    }
  }

}
