import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExampleRoutingModule } from './form-routing.module';
import { FormComponent } from './form.component';
import { UtilityModule } from '@app/utility/utility.module';
import { SharedModule } from '@app/shared/shared.module';


@NgModule({
  declarations: [
    FormComponent
  ],
  imports: [
    CommonModule,
    UtilityModule,
    SharedModule,
    ExampleRoutingModule
  ]
})
export class ExampleModule { }
