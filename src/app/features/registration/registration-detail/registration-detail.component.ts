import { Component, OnInit } from '@angular/core';
import { Sort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';
import { DEFAULT_LIMIT, DEFAULT_OFFSET } from '@app/core/constants/constants';
import { NotificationService } from '@app/core/services/notification.service';
import { QueryParams } from '@app/models';
import { Pageable, TableColumn } from '@app/utility/utils';
import { FormDetail } from '@app/utility/utils/form';
import { finalize, ReplaySubject, takeUntil } from 'rxjs';
import { DepartmentService } from '../services/department.service';

@Component({
  selector: 'app-registration-detail',
  templateUrl: './registration-detail.component.html',
  styleUrls: ['./registration-detail.component.css']
})
export class RegistrationDetailComponent implements OnInit {

  public result: any;
  public details: FormDetail[];
  oid: string;
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  isLoading: boolean = false;

  columns: TableColumn[] = [
    { columnDef: 'user_name', columnDefBn: 'user_name_bn', header: 'Member Name' },
    { columnDef: 'email', columnDefBn: 'email', header: 'Email' },
    { columnDef: 'mobile_number', columnDefBn: 'mobile_number', header: 'Mobile Number' },
    { columnDef: 'status', columnDefBn: 'status', header: 'Status' }
  ];

  data: any[] = [];
  totalCount = 0;
  searchText: string = '';
  resetPagination: boolean = false;

  limit: number = DEFAULT_LIMIT;
  offset: number = DEFAULT_OFFSET;

  constructor(private departmentService: DepartmentService,
    private router: Router,
    private notificationService: NotificationService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.oid = this.route.snapshot.params["id"];
    this.getDepartmentDetail();
  }

  getList() {
    this.router.navigate(['/department']);
  }

  getDepartmentDetail(offset = DEFAULT_OFFSET, limit = DEFAULT_LIMIT, searchText: string = '', sortOrderId: string = 'created_on', 
  sortOrder: string = 'desc') {
    this.isLoading = true;
    let params = new QueryParams(offset, limit, '', searchText, sortOrder, sortOrderId);
    console.log(this.oid)
    this.departmentService.getDepartmentByOid(this.oid, params)
      .pipe(takeUntil(this.destroyed$), finalize(() => { this.isLoading = false; }))
      .subscribe({
        next: (res: any) => {
          this.result = res.data;
          console.log(this.result)
          // this.data = res.data.members;
          // this.totalCount = res.data.memberCount;
          this.details = [
            {
              label: 'Registration Id',
              valueBn: this.result.registrationId,
              valueEn: this.result.registrationId,
              type: 'Normal',
            },
            {
              label: 'First Name',
              valueBn: this.result.firstName,
              valueEn: this.result.firstName,
              type: 'Normal',
            },
            {
              label: 'Last Name',
              valueBn: this.result.lastName,
              valueEn: this.result.lastName,
              type: 'Normal',
            },
            {
              label: 'Email',
              valueBn: this.result.email,
              valueEn: this.result.email,
              type: 'Normal',
            },
            {
              label: 'Mobile',
              valueBn: this.result.mobile,
              valueEn: this.result.mobile,
              type: 'Normal',
            },
            {
              label: 'Date Of Birth',
              valueBn: this.result.dateOfBirth,
              valueEn: this.result.dateOfBirth,
              type: 'Normal',
            },
            {
              label: 'Address',
              valueBn: this.result.address,
              valueEn: this.result.address,
              type: 'Normal',
            },
            {
              label: 'Status',
              valueBn: this.result.status,
              valueEn: this.result.status,
              type: 'Normal',
            },
          ]
        },
        error: (err) => {
          this.notificationService.errorSnackBar(err);
        },
        complete: () => { }
      })

  }

  pageChangeEvent(event: Pageable): void {
    this.limit = event.limit;
    this.offset = event.offset;
    // this.getDepartmentListByTeamLeadOid(event.offset, event.limit, this.searchText);
  }

  pageSortEvent(event: Sort): void {
    // this.getDepartmentListByTeamLeadOid(this.offset, this.limit, this.searchText, event.active, event.direction);
  }

  onTableAction(event: any): void {
    console.log('event', event);
  }

  onSearch(searchText: string) {
    if (searchText) {
      this.resetPagination = true;
    } else {
      this.resetPagination = false;
    }
    this.searchText = searchText;
    // this.getDepartmentListByTeamLeadOid(DEFAULT_OFFSET, this.limit, searchText);
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

}
