import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from '@app/core/services/notification.service';
import { ControlItem, QueryParams } from '@app/models';
import { markFormGroupTouched } from '@app/utility/utils/form';
import { TranslateService } from '@ngx-translate/core';
import { finalize, map, ReplaySubject, takeUntil } from 'rxjs';
import { DepartmentService } from '../services/department.service';
import { DEFAULT_LIMIT, DEFAULT_OFFSET } from '@app/core/constants/constants';

@Component({
  selector: 'app-update-registration',
  templateUrl: './update-registration.component.html',
  styleUrls: ['./update-registration.component.css']
})
export class UpdateRegistrationComponent implements OnInit {

  public regOid: string;
  isLoading: boolean = false;
  form: FormGroup;
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  teamMembers: ControlItem[];
  isInline: boolean;
  teamLeads: ControlItem[];
  projects: ControlItem[];

  limit: number = DEFAULT_LIMIT;
  offset: number = DEFAULT_OFFSET;

  items: ControlItem[] = [
    { label: 'Active', labelBn: 'Active', value: 'Active' },
    { label: 'Inactive', labelBn: 'Inactive', value: 'Inactive' }
  ];

  constructor(
    private departmentService: DepartmentService,
    private router: Router,
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private route: ActivatedRoute,
    private translate: TranslateService) { }

  currentLanguage: any;

  ngOnInit(): void {
    this.regOid = this.route.snapshot.params["id"];
    this.getDepartmentDetail();

    this.currentLanguage = this.translate.currentLang;

    this.translate.onLangChange.subscribe((s: { lang: string }) => {
      if (s.lang === 'en') {
        this.currentLanguage = 'en';
      } else {
        this.currentLanguage = 'bn';
      }
    });
  }

  getDepartmentDetail(offset = DEFAULT_OFFSET, limit = DEFAULT_LIMIT, searchText: string = '', sortOrderId: string = 'created_on', 
  sortOrder: string = 'desc') {
    let params = new QueryParams(offset, limit, '', searchText, sortOrder, sortOrderId);
    this.departmentService.getDepartmentByOid(this.regOid, params)
      .pipe(takeUntil(this.destroyed$), finalize(() => { this.isLoading = false; }))
      .subscribe({
        next: (res: any) => {
          console.log(res)
          this.form = this.fb.group({
            firstName: [res.data.firstName, {
              updateOn: 'change', validators: [
                Validators.required
              ]
            }],
            lastName: [res.data.lastName, {
              updateOn: 'change', validators: [
                Validators.required
              ]
            }],
            email: [res.data.email, {
              updateOn: 'change', validators: [
                Validators.required
              ]
            }],
            mobile: [res.data.mobile, {
              updateOn: 'change', validators: [
                Validators.required
              ]
            }],
            dateOfBirth: [res.data.dateOfBirth, {
              updateOn: 'change', validators: [
                Validators.required
              ]
            }],
            address: [res.data.address, {
              updateOn: 'change', validators: [
                Validators.required
              ]
            }],
            // userId: [res.data.userId, {
            //   updateOn: 'change', validators: [
            //     Validators.required
            //   ]
            // }],
            // password: [
            //   res.data.password, {
            //     updateOn: "change",
            //     validators: [Validators.required],
            //   }]
          });
        },
        error: (err) => {
          this.notificationService.errorSnackBar(err);
        },
        complete: () => { }
      })
  }



  getList() {
    this.router.navigate(['/registration']);
  }

  onSubmit() {
    if (this.form.valid) {
      let myData = this.form.value;
      myData.oid = this.regOid;
      console.log(myData)
      this.departmentService.updateDepartment(myData).subscribe({
        next: (res: any) => {
          console.log(res)
          if (res.code === 201) {
            this.notificationService.successSnackBar("Registration has been Updated successfully");
            this.getList();
          } else {
            this.notificationService.infoSnackBar(res.message);
          }
        },
        error: (err) => {
          this.notificationService.errorSnackBar(err);
        },
        complete: () => { }
      });
    } else {
      markFormGroupTouched(this.form);
    }
  }

}
