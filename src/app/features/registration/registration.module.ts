import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@app/shared/shared.module';
import { UtilityModule } from '@app/utility/utility.module';
import { MaterialModule } from '@app/shared/material.module';
import { RegistrationComponent } from './registration.component';
import { AddRegistrationComponent } from './add-registration/add-registration.component';
import { RegistrationDetailComponent } from './registration-detail/registration-detail.component';
import { RegistrationRoutingModule } from './registration-routing.module';
import { UpdateRegistrationComponent } from './update-registration/update-registration.component';


@NgModule({
  declarations: [
    RegistrationComponent,
    AddRegistrationComponent,
    RegistrationDetailComponent,
    UpdateRegistrationComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    UtilityModule,
    MaterialModule,
    RegistrationRoutingModule
  ]
})
export class RegistrationModule { }
