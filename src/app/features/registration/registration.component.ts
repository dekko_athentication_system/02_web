import { Component, OnInit } from '@angular/core';
import { Sort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';
import { DEFAULT_LIMIT, DEFAULT_OFFSET } from '@app/core/constants/constants';
import { NotificationService } from '@app/core/services/notification.service';
import { QueryParams } from '@app/models';
import { Pageable, TableColumn } from '@app/utility/utils';
import { IButtonDescription } from '@app/utility/utils/button-description';
import { finalize, ReplaySubject, takeUntil } from 'rxjs';
import { DepartmentService } from './services/department.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  listButton: IButtonDescription[] = [
    {listener: row => this.getView(row),
      text: '',
      toolTip:'View',
      icon: 'remove_red_eye'
    },
    {listener: row => this.getViewForUpdating(row),
      text: '',
      toolTip:'Edit',
      icon: 'edit'
    }
  ];

  getView(data: any){
    this.router.navigate([data.oid], { relativeTo: this.activatedRoute })
  }

  getViewForUpdating(data: any){
    this.router.navigate(['edit/'+data.oid], { relativeTo: this.activatedRoute })
  }

  columns: TableColumn[] = [
    { columnDef: 'registrationId', columnDefBn: 'registrationId', header: 'Registration Id' },
    { columnDef: 'firstName', columnDefBn: 'firstName', header: 'First Name' },
    { columnDef: 'lastName', columnDefBn: 'lastName', header: 'Last Name' },
    { columnDef: 'email', columnDefBn: 'email', header: 'Email' },
    { columnDef: 'mobile', columnDefBn: 'mobile', header: 'Mobile' },
    { columnDef: 'address', columnDefBn: 'address', header: 'Address' },
    { columnDef: 'dateOfBirth', columnDefBn: 'dateOfBirth', header: 'Date Of Birth' },
    { columnDef: 'status',columnDefBn: 'status', header: 'Status' }
  ]; 

  isLoading: boolean = false;
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  data: any[] = [];
  totalCount = 0;
  searchText: string = '';
  resetPagination: boolean = false;
  

  limit: number = DEFAULT_LIMIT;

  constructor(private departmentService: DepartmentService, private notificationService: NotificationService,
              private router: Router, private activatedRoute: ActivatedRoute) { }

  

  ngOnInit(): void {
    this.getDepartmentList();
  }

  getDepartmentList(offset = DEFAULT_OFFSET, limit = DEFAULT_LIMIT, searchText: string = '', 
  sortOrderId: string= 'created_on', sortOrder: string = 'desc'){
    this.isLoading = true;
    let params = new QueryParams(offset, limit, '', searchText, sortOrder, sortOrderId);
    this.departmentService.getDepartmentList(params)
    .pipe(takeUntil(this.destroyed$), finalize(() => { this.isLoading = false;}))
      .subscribe({
        next: (res: any) => {
          this.data = res.data;
          this.totalCount = res.count;
        },
        error: (err) => {
          this.notificationService.errorSnackBar(err);
        },
        complete: () => {} 
      });
  }

  AddType() {
    this.router.navigate(['add'], { relativeTo: this.activatedRoute })
}

pageChangeEvent(event: Pageable): void  {
  this.limit = event.limit;
  this.getDepartmentList(event.offset, event.limit, this.searchText);
}

pageSortEvent(event: Sort): void  {
  this.getDepartmentList(DEFAULT_OFFSET, DEFAULT_LIMIT, this.searchText, event.active, event.direction);
}

onTableAction(event: any): void {
  console.log('event', event);
}

onSearch(searchText: string) {
  if(searchText) {
    this.resetPagination = true;
  } else {
    this.resetPagination = false;
  }
  this.searchText = searchText;
  this.getDepartmentList(DEFAULT_OFFSET, this.limit, searchText);
}

ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
}

}
