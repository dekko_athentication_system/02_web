import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegistrationComponent } from './registration.component';
import { RegistrationDetailComponent } from './registration-detail/registration-detail.component';
import { AddRegistrationComponent } from './add-registration/add-registration.component';
import { UpdateRegistrationComponent } from './update-registration/update-registration.component';

const routes: Routes = [
  {
    path: '',
    component: RegistrationComponent
  },
  {
    path: 'add',
    component: AddRegistrationComponent
  },
  {
    path: ':id',
    component: RegistrationDetailComponent
  },
  {
    path: 'edit/:id',
    component: UpdateRegistrationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistrationRoutingModule { }
