import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ResourceService } from '@app/core/services/resouce.service';
import { QueryParams } from '@app/models';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService extends ResourceService<any> {

  constructor(private http: HttpClient) {
    super(http);
  }

  getDepartmentList(params: QueryParams): Observable<any> {
    return this.getListWithPage(params, 'v1/registration/list');
  }

  getDepartmentByOid(oid: String, params: QueryParams): Observable<any> {
    return this.getListByPostWithPage(params, {oid: oid}, 'v1/registration/' + oid);
  }

  saveDepartment(data: any): Observable<any> {
    return this.post(data, 'v1/registration/save');
  }

  updateDepartment(myData: any): Observable<any> {
    return this.post(myData,  'v1/registration/edit/' + myData.oid);
  }

  // updateDepartment(myData: any): Observable<any> {
  //   return this.put(myData, 'v1/update-department-by-oid ');
  // }




  getDepartmentListWithoutPage(): Observable<any> {
    return this.getList('v1/department-list');
  }

  getDepartmentListByProjectOid(projectOid: String): Observable<any> {
    return this.post({ projectOid: projectOid }, 'v1/department-list-by-project');
  }

  // saveDepartment(data: any): Observable<any> {
  //   return this.post(data, 'v1/save-department');
  // }

  // getDepartmentByOid(oid: String, params: QueryParams): Observable<any> {
  //   return this.getListByPostWithPage(params,{ oid: oid }, 'v1/registration');
  // }

  // getDepartmentByOidForUpdating(oid: String): Observable<any> {
  //   return this.post({ oid: oid }, 'v1/registration');
  // }

  // updateDepartment(myData: any): Observable<any> {
  //   return this.put(myData, 'v1/update-department-by-oid ');
  // }

  getDepartmentListByTeamMemberOid(params: QueryParams): Observable<any> {
    return this.getListWithPage(params, 'v1/department-list-by-member-oid');
  }

  getDepartmentListByTeamLeadOid(params: QueryParams): Observable<any> {
    return this.getListWithPage(params, 'v1/department-list-by-team-lead-oid');
  }

  getDepartmentByIssueOid(issueOid: String): Observable<any> {
    return this.post({ issueOid: issueOid }, 'v1/department-view-by-issue-oid');
  }

  updateDepartmentMemberByAdmin(data: any): Observable<any> {
    return this.post(data, 'v1/team-member-adding-by-team-lead');
  }

}
