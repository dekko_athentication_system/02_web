import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from '@app/core/services/notification.service';
import { ControlItem } from '@app/models';
import { markFormGroupTouched } from '@app/utility/utils/form';
import { regex, regexErrors } from '@app/utility/utils/regex';
import { TranslateService } from '@ngx-translate/core';
import { finalize, map, ReplaySubject, takeUntil, tap } from 'rxjs';
import { DepartmentService } from '../services/department.service';

@Component({
  selector: 'app-add-registration',
  templateUrl: './add-registration.component.html',
  styleUrls: ['./add-registration.component.css']
})
export class AddRegistrationComponent implements OnInit {

  form: FormGroup;
  regexErrors = regexErrors;
  isInline: boolean;
  projects: ControlItem[];
  teamLeads: ControlItem[];
  teamMembers: ControlItem[];
  isLoading: boolean = false;

  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);

  constructor(private notificationService: NotificationService,
    private fb: FormBuilder,
    private router: Router,
    private _departmentService: DepartmentService,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute
  ) {
    this.isInline = false;
  }

  currentLanguage: any;
  ngOnInit(): void {
    this.createform();
    // this.getTeamLeadList();
    // this.getMemberList();

    this.currentLanguage = this.translate.currentLang;

    this.translate.onLangChange.subscribe((s: { lang: string }) => {
      if (s.lang === 'en') {
        this.currentLanguage = 'en';
      } else {
        this.currentLanguage = 'bn';
      }
    });
  }

  // onInputPaste(event) {
  //   var clipboardData = event.clipboardData || window.clipboardData;
  //   if (/^[0-9০-৯]+$/g.test(clipboardData.getData("Text"))) return;
  //   event.stopPropagation();
  //   event.preventDefault();
  // }

  keyPressAlphanumeric(event: any) {
    var inp = String.fromCharCode(event.keyCode);
    if (/^[\d\u0980-\u09FF]+$/g.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }

  createform() {
    this.form = this.fb.group({
      firstName: [null, {
        updateOn: 'blur', validators: [
          Validators.required
        ]
      }],
      lastName: [null, {
        updateOn: 'change', validators: [
          Validators.required
        ]
      }],
      email: [null, {
        updateOn: 'change', validators: [
          Validators.required
        ]
      }],
      mobile: [null, {
        updateOn: 'change', validators: [
          Validators.required
        ]
      }],
      dateOfBirth: [null, {
        updateOn: 'change', validators: null
      }],
      address: [null, {
        updateOn: 'change', validators: [
          Validators.required
        ]
      }],
      userId: [null, {
        updateOn: 'change', validators: [
          Validators.required
        ]
      }],
      password: [null, {
        updateOn: 'change', validators: [
          Validators.required
        ]
      }]
    });
  }

  public onSubmit(): void {
    if (this.form.valid) {
      this._departmentService.saveDepartment(this.form.value).subscribe({
        next: (res: any) => {
          if (res.code === 201) {
            this.notificationService.successSnackBar("Registration has been added successfully");
            // this.form.reset();
            // this.createform();
            this.router.navigate(['/registration']);
            // this.router.navigate(['/registration'], { relativeTo: this.activatedRoute })
          } else {
            this.notificationService.infoSnackBar(res.message);
          }
        },
        error: (err) => {
          this.notificationService.errorSnackBar(err);
        },
        complete: () => { }
      });
    } else {
      markFormGroupTouched(this.form);
    }
  }



  // public getTeamLeadList() {
  //   this.isLoading = true;
  //   this._userInfoService.getTeamLeadList()
  //     .pipe(
  //       takeUntil(this.destroyed$),
  //       map((items) =>
  //         items.data.map((i: any) => ({ label: i.user_name, value: i.oid, labelBn: i.user_name_bn }))
  //       ),
  //       finalize(() => {
  //         this.isLoading = false;
  //       })
  //     )
  //     .subscribe((res) => {
  //       this.teamLeads = res;
  //     });
  // }

  getList() {
    this.router.navigate(['/registration']);
  }

}
