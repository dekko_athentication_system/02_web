import { Injectable } from '@angular/core';
import { SharedService } from '@app/core/services/shared.service';
import { Observable } from 'rxjs';
import { io, Socket } from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  socket: Socket;

  url = 'https://ticketing.celloscope.net'

  // url = 'http://localhost:3000'


  constructor( private sharedService: SharedService) {
    this.socket = io(this.url, {
      query: {"extension": this.sharedService.getUserExtension()},
      path: '/mysocket'
    });
   }

   listen(eventName: any): Observable<any>{
    return new Observable(ovserver => {
      this.socket.on(eventName, (data: any) => {
        ovserver.next(data);
      });
    });
   }

   emit(eventName: any, data: any){
     this.socket.emit(eventName, data);
   }

   disconnect(){
     this.socket.disconnect();
   }
}
